﻿using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections;

public class TitleController : MonoBehaviour {

	[SerializeField] Animator m_Animator;
	[SerializeField] ScrollRect m_ScrollRect;
	[SerializeField] RectTransform m_Contents;
	[SerializeField] RectTransform m_EndingPanel;
    [Space]
    [SerializeField] RectTransform m_OptionPanel;
    [SerializeField] Slider m_BGMSlider;
    [SerializeField] Slider m_SESlider;
    [Space]
    [SerializeField] AudioClip m_OpenSE;
    [SerializeField] AudioClip m_CloseSE;


	AudioSource m_AudioSource;
	bool m_IsOpenHowToPlay = false;
	int m_HowToPlayIndex = 0;
	float m_PageMoveSpeed = 10f;

	void Awake(){
		m_AudioSource = GetComponent<AudioSource> ();
	}

	void Update(){
		m_AudioSource.volume = GameManager.Instance.SEVolume;
		SetHowToPlay ();

		if (Input.anyKeyDown && m_IsOpenHowToPlay) {
			MoveNextPage ();
		}
	}

    public void PlayOpenSE() {
        m_AudioSource.PlayOneShot(m_OpenSE);
    }

    public void PlayCloseSE() {
        m_AudioSource.PlayOneShot(m_CloseSE);
    }

    public void OpenOption() {
        m_BGMSlider.value = GameManager.Instance.BGMVolume;
        m_SESlider.value = GameManager.Instance.SEVolume;
        m_OptionPanel.gameObject.SetActive(true);
    }

    public void SetVolume() {
        if (m_OptionPanel.gameObject.activeInHierarchy) {
            GameManager.Instance.BGMVolume = m_BGMSlider.value;
            GameManager.Instance.SEVolume = m_SESlider.value;
        }
    }

	void SetHowToPlay(){
		if (m_IsOpenHowToPlay) {
			m_ScrollRect.horizontalNormalizedPosition = 
				Mathf.Lerp (
				m_ScrollRect.horizontalNormalizedPosition, 1, 
				Time.deltaTime * m_PageMoveSpeed);
		} else {
			m_ScrollRect.horizontalNormalizedPosition = 0f;
		}

		m_ScrollRect.transform.localScale = 
			Vector3.Lerp (
				m_ScrollRect.transform.localScale, 
				m_IsOpenHowToPlay ? Vector3.one : Vector3.zero, 
				Time.deltaTime * m_PageMoveSpeed);
	}

	public void MoveNextPage(){
		if (m_IsOpenHowToPlay) {
			m_HowToPlayIndex++;
			if (m_HowToPlayIndex >= m_Contents.childCount) {
				CloseHowToPlay ();
				return;
			}
			m_Contents.GetChild (m_HowToPlayIndex).gameObject.SetActive (true);
		}
	}

	public void OpenHowToPlay(){
		if (!m_IsOpenHowToPlay) {
			m_IsOpenHowToPlay = true;
			m_Contents.GetChild (0).gameObject.SetActive (true);
			m_Contents.GetChild (m_Contents.childCount - 1).gameObject.SetActive (false);
		}
	}

	void CloseHowToPlay(){
		m_HowToPlayIndex = 0;
		for (int i = 0; i < m_Contents.childCount - 1; i++) {
			m_Contents.GetChild (i).gameObject.SetActive (false);
		}
		m_IsOpenHowToPlay = false;
		m_ScrollRect.horizontalNormalizedPosition = 0f;
	}

	public void SceneChange(string sceneName){
		GameManager.Instance.SceneChange (sceneName, 2f, Color.black);
	}

	public void Quit(){
		StartCoroutine (QuitCoroutine ());
	}

	IEnumerator QuitCoroutine(){
		m_EndingPanel.gameObject.SetActive (true);
		yield return new WaitForSeconds (3f);
		GameManager.Instance.FadeOut (2f, 1f, Color.black);
		yield return new WaitForSeconds (1f);
		Application.Quit ();
	}
}
