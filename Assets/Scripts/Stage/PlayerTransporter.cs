﻿using UnityEngine;
using System.Collections;

public class PlayerTransporter : MonoBehaviour {

	[SerializeField] public bool IsTransportImmediately = false;
	[SerializeField] Transform m_PivotTransform;
	[SerializeField] Transform m_TargetTransform;
	[SerializeField] float m_TransportTime;

	Collider2D m_Collider;

	void Awake(){
		m_Collider = transform.GetChild(0).GetComponent<Collider2D>();
	}

	public void Transport(Transform _transform){
		StartCoroutine (TransportCoroutine (_transform));
	}

	IEnumerator TransportCoroutine(Transform _transform){
		UIManager.Instance.Blackout (m_TransportTime, Color.black);
		m_Collider.enabled = false;
		yield return new WaitForSeconds (m_TransportTime / 2);
		_transform.position = m_TargetTransform.position;
		m_Collider.enabled = true;
	}

	void OnDrawGizmos(){

		if (m_PivotTransform  != null && m_TargetTransform != null) {
			Gizmos.color = Color.green;
			Gizmos.DrawLine (m_PivotTransform.position, m_TargetTransform.position);
		}
	}

}
