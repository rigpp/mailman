﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

	[HideInInspector] bool CanPickUp = true;
	[SerializeField] ItemType m_ItemType;
	[SerializeField] SpriteRenderer m_ItemSpriteRenderer;
	Collider2D m_Collider;

	void Awake(){
		m_Collider = transform.GetChild (0).gameObject.GetComponent<Collider2D> ();
	}

	public bool PickUp(out ItemType type){
		type = m_ItemType;
		if (CanPickUp) {
			CanPickUp = false;
			m_Collider.enabled = false;
			StartCoroutine (FadeOutCoroutine (1f));
			return true;
		} else {
			return false;
		}
	}

	IEnumerator FadeOutCoroutine(float time){
		float t = 0;
		float deltaAlpha = 1f / time;
		Color c = m_ItemSpriteRenderer.color;
		while (t <= time) {
			c.a -= deltaAlpha * Time.deltaTime;
			m_ItemSpriteRenderer.color = c;
			t += Time.deltaTime;
			yield return null;
		}
		Destroy (gameObject);
	}

}
