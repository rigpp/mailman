﻿using UnityEngine;
using System.Collections;

public class StepController : MonoBehaviour {

	[SerializeField] float m_UpOffset;
	[SerializeField] Collider2D m_Collider;

	Transform m_PlayerTransform;

	void Start(){
		m_PlayerTransform = GameObject.FindGameObjectWithTag ("Player").transform;
		m_Collider.enabled = false;
	}

	void Update(){
		if ((m_Collider.transform.position.y + m_UpOffset) < m_PlayerTransform.position.y) {
			m_Collider.enabled = true;
		} else if((m_Collider.transform.position.y - m_UpOffset) > m_PlayerTransform.position.y){
			m_Collider.enabled = false;
		}
		Debug.Log (gameObject.name + ":" + m_Collider.enabled);
	}

}
