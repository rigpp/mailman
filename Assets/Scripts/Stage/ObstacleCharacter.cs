﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class ObstacleCharacter : MonoBehaviour {

	[SerializeField] GameObject m_Icon;
	[SerializeField] Collider2D m_ObstacleCollider;
	[SerializeField] Collider2D m_Trigger;
	[SerializeField] UnityEvent m_OnLetterGettng;
	bool m_IsReceivedLetter = false;

	public bool  DeliverLetter(ref int letterCount){
		if (letterCount <= 0 || m_IsReceivedLetter) {
			return false;
		} else {
			letterCount--;
			m_IsReceivedLetter = true;
			m_Trigger.enabled = false;
		}
		m_OnLetterGettng.Invoke ();
		UIManager.Instance.Blackout (0.5f, Color.black);
		Invoke ("MoveOver", 0.25f);
		return true;
	}

	void MoveOver(){
		m_ObstacleCollider.enabled = false;
		m_Icon.SetActive (false);
	}
		
}
