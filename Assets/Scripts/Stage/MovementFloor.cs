﻿using UnityEngine;
using System.Collections;

public class MovementFloor : MonoBehaviour {

	[SerializeField] Vector2 m_MoveSpeed;
	bool m_IsMove = false;
	Transform m_Player;
	float m_PrePositionX;

	void Update(){
		if (m_IsMove && (m_Player.position.y >= transform.position.y + 1f)) {
			transform.Translate (m_MoveSpeed * Time.deltaTime);
		}
	}
		
	void LateUpdate(){
		float deltaPosition = transform.position.x - m_PrePositionX;
		if (m_IsMove) {
			Vector2 newPos = m_Player.position;
			newPos.x += deltaPosition;
			m_Player.position = newPos;
		}
		m_PrePositionX = transform.position.x;
	}

	void OnCollisionEnter2D(Collision2D _other){
		if (_other.gameObject.tag == "Player") {
			m_Player = _other.gameObject.transform;
			m_IsMove = true;
		}
	}

	void OnCollisionExit2D(Collision2D _other){
		if (_other.gameObject.tag == "Player") {
			m_Player = null;
			m_IsMove = false;
		}
	}

}
