﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;


public class UIManager : MonoBehaviour {

	public static UIManager Instance;

	[SerializeField] Image m_BlackoutImage;
	[HideInInspector] public bool m_IsBlackouting = false;
	[SerializeField] Text m_CurrentTimeText;
	[SerializeField] Text m_MessageText;
	[SerializeField] Text m_LetterCountText;
	[Space]
	[SerializeField] RectTransform m_PausePanel;
	[SerializeField] Slider m_BGMSlider;
	[SerializeField] Slider m_SESlider;
	[Space]
	[SerializeField] AudioClip m_OpenSE;
	[SerializeField] AudioClip m_CloseSE;

	//const string c_StageScelectSceneName = "TitleTestScene0";
	AudioSource m_AudioSource;

	void Awake(){
		Instance = this;
		m_AudioSource = GetComponent<AudioSource> ();
	}

	public void PlayCloseSound(){
		m_AudioSource.volume = GameManager.Instance.SEVolume;
		m_AudioSource.PlayOneShot (m_CloseSE);
	}
	public void PlayOpenSound(){
		m_AudioSource.volume = GameManager.Instance.SEVolume;
		m_AudioSource.PlayOneShot (m_OpenSE);
	}

	public void SetPausePanel(bool enable){
		if (enable) {
			m_BGMSlider.value = GameManager.Instance.BGMVolume;
			m_SESlider.value = GameManager.Instance.SEVolume;
			PlayOpenSound ();
		} else {
			PlayCloseSound ();
		}
		GameManager.Instance.IsPause = enable;
		m_PausePanel.gameObject.SetActive (enable);
	}

	public void SetVolume(){
		if (m_BGMSlider.gameObject.activeInHierarchy) {
			GameManager.Instance.BGMVolume = m_BGMSlider.value;
			GameManager.Instance.SEVolume = m_SESlider.value;
		}
	}

	public void Restart(){
		GameManager.Instance.IsPause = false;
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

	public void BackStageSelect(){
		GameManager.Instance.IsPause = false;
		PlayCloseSound ();	
		GameManager.Instance.SceneChange (GameManager.Instance.StageScelectSceneName, 2f, Color.black);
	}
		
	public void SetCurrentTime(float currentTime){
		int millisecond = (int)Mathf.Floor((currentTime - Mathf.Floor(currentTime)) * 100)	;
		m_CurrentTimeText.text = System.String.Format ("{0:00}:{1:00}:{2:00}", (int)currentTime / 60, (int)currentTime % 60,millisecond);
	}

	public void SetMessageText(string text, Color color,bool enable){
		m_MessageText.text = text;
		m_MessageText.color = color;
		m_MessageText.gameObject.SetActive (enable);
	}
	public void SetMessageText(string text, Color color, float time){
		StartCoroutine (SetMessageCoroutine (text,color,time));
	}

	IEnumerator SetMessageCoroutine(string text,Color color ,float time){
		m_MessageText.gameObject.SetActive (true);
		m_MessageText.text = text;
		color.a = 0;
		m_MessageText.color = color;
		float t = 0;
		float deltaAlpha = 1f / (time / 2);
		Color c = m_MessageText.color;
		while(t <= time / 2f){
			c.a += deltaAlpha * Time.deltaTime;;
			m_MessageText.color= c;
			t += Time.deltaTime;
			yield return null;
		}
		/*
		t = 0;
		c = m_BlackoutImage.color;
		yield return null;
		while(t <= time / 2f){
			c.a -= deltaAlpha * Time.deltaTime;
			m_BlackoutImage.color = c;
			t += Time.deltaTime;
			yield return null;
		}
		m_BlackoutImage.gameObject.SetActive (false);
		*/
	}


	public void SetLetterCountText(int count){
		m_LetterCountText.text = ":" + count;
	}

	public void Blackout(float time,Color color){
		if (!m_IsBlackouting) {
			StartCoroutine (BlackoutCoroutine (time,color));
		}
	}

	IEnumerator BlackoutCoroutine(float time,Color color){
		m_IsBlackouting = true;
		m_BlackoutImage.gameObject.SetActive (true);
		color.a = 0;
		m_BlackoutImage.color = color;
		float t = 0;
		float deltaAlpha = 1f / (time / 2);
		Color c = m_BlackoutImage.color;
		while(t <= time / 2f){
			c.a += deltaAlpha * Time.deltaTime;
			m_BlackoutImage.color= c;
			t += Time.deltaTime;
			yield return null;
		}
		t = 0;
		c = m_BlackoutImage.color;
		yield return null;
		while(t <= time / 2f){
			c.a -= deltaAlpha * Time.deltaTime;
			m_BlackoutImage.color = c;
			t += Time.deltaTime;
			yield return null;
		}
		m_BlackoutImage.gameObject.SetActive (false);
		m_IsBlackouting = false;
	}

}
