﻿using UnityEngine;
using UnityStandardAssets._2D;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections;

public class PlayerManager : MonoBehaviour {

	[HideInInspector] public bool IsMove = true;
	[HideInInspector] public PlayerFreezeMode playerFreezeMode = PlayerFreezeMode.None;

	[SerializeField] Animator m_IconAnimator;
	[SerializeField] AudioClip m_ActionSE;
	Platformer2DUserControl m_Control;
	PlatformerCharacter2D m_Charactor;
	Animator m_Animator;
	Rigidbody2D m_Rigidbody2D;
	AudioSource m_AudioSource;
	int m_LetterCount = 0;
	bool m_IsIconPopUp = false;
	bool m_IsFinished = false;

	void Awake(){
		m_Control = GetComponent<Platformer2DUserControl> ();
		m_Animator = GetComponent<Animator> ();
		m_Rigidbody2D = GetComponent<Rigidbody2D> ();
		m_AudioSource = GetComponent<AudioSource> ();
	}

	void Start(){
		StageManager.Instance.SetPlayerManager (this);
	}

	void Update(){

		bool canMove = !UIManager.Instance.m_IsBlackouting && !GameManager.Instance.IsSceneChangeing && IsMove;
		if (!canMove) {
			m_Control.enabled = false;
			m_Rigidbody2D.constraints = m_IsFinished ? RigidbodyConstraints2D.FreezeRotation : RigidbodyConstraints2D.FreezeAll;
			m_Animator.SetFloat("Speed",0f);
		} else {
			m_Control.enabled = true;
			m_Rigidbody2D.constraints = RigidbodyConstraints2D.None;
			m_Rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
		}

		UIManager.Instance.SetLetterCountText (m_LetterCount);
		m_IconAnimator.SetBool ("IconPopUp", m_IsIconPopUp);
		m_Control.CanJump = !m_IsIconPopUp;

		m_AudioSource.volume = GameManager.Instance.SEVolume;
	}

	void FixedUpdate(){
		m_IsIconPopUp = false;
	}

	void OnTriggerEnter2D(Collider2D _other){
		if (_other.tag == "Finish" && IsMove) {
			StageManager.Instance.IsTimeCount = false;
			Invoke ("GameFinish",0.5f);
			Invoke ("BackStageSelect", 5f);
		}
	}

	void GameFinish(){
		IsMove = false;
		m_IsFinished = true;
		m_Rigidbody2D.drag = 5;
		UIManager.Instance.SetMessageText ("Goal", Color.cyan,0.5f);
	}

	void BackStageSelect(){
		GameManager.Instance.SceneChange (GameManager.Instance.StageScelectSceneName, 2f, Color.black);
	}
		
	void OnTriggerStay2D(Collider2D _other){
		m_IsIconPopUp = _other.tag != "Finish" ? true : false;

		Item item = _other.transform.root.GetComponent<Item> ();
		ItemType type = ItemType.None;
		if (item != null && CrossPlatformInputManager.GetButtonDown("Submit")) {
			if (item.PickUp (out type)) {
				m_AudioSource.PlayOneShot (m_ActionSE);
				switch (type) {
				case ItemType.Letter:
					m_LetterCount++;
					break;
				}
			}
		}

		if (_other.transform.parent != null) {
			PlayerTransporter transporter = _other.transform.parent.GetComponent<PlayerTransporter> ();
			if (transporter != null) {
				if (transporter.IsTransportImmediately) {
					transporter.Transport (transform);
				} else {
					if (CrossPlatformInputManager.GetButtonDown ("Submit")) {
						transporter.Transport (transform);
					}
				}
			}
		}

		ObstacleCharacter obstacle = _other.transform.root.GetComponent<ObstacleCharacter> ();
		if (obstacle != null && CrossPlatformInputManager.GetButtonDown("Submit")) {
			if(obstacle.DeliverLetter (ref m_LetterCount)){
				m_AudioSource.PlayOneShot (m_ActionSE);
			}
		}
	}


}
