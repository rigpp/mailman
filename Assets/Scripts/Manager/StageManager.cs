﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class StageManager : MonoBehaviour {

	public static StageManager Instance;
	[HideInInspector] public bool IsTimeCount = true;

	[SerializeField] int m_StageIndex = 0;
	[SerializeField] float m_TimeLimit;
	AudioSource m_AudioSource;
	PlayerManager m_PlayerManager;
	float m_CurrenTime = 0;
	bool m_OnTimeLimitHappen = false;

	void Awake(){
		Instance = this;
		m_AudioSource = GetComponent<AudioSource> ();
	}

	void Start(){
		m_CurrenTime = m_TimeLimit;
		GameManager.Instance.ResentPlayStageIndex = m_StageIndex;
	}

	void Update(){
		bool canTimeCount = !GameManager.Instance.IsSceneChangeing && IsTimeCount;
		if (m_CurrenTime >= 0) {
			if (canTimeCount) {
				m_CurrenTime -= Time.deltaTime;
				UIManager.Instance.SetCurrentTime (m_CurrenTime);
			}
		}else if(!m_OnTimeLimitHappen){
			m_OnTimeLimitHappen = true;
			UIManager.Instance.SetCurrentTime (0f);	
			UIManager.Instance.SetMessageText ("Time Over", Color.red, 0.5f);
			Invoke ("BackStageSelect", 5f);
			m_PlayerManager.IsMove = false;
		}

		if (CrossPlatformInputManager.GetButtonDown ("Cancel") && !GameManager.Instance.IsPause && !GameManager.Instance.IsSceneChangeing) {
			UIManager.Instance.SetPausePanel (true);
		}

		m_AudioSource.volume = GameManager.Instance.BGMVolume;
	}

	void BackStageSelect(){
		GameManager.Instance.SceneChange (GameManager.Instance.StageScelectSceneName, 2f, Color.black);
	}

	public void SetPlayerManager(PlayerManager player){
		m_PlayerManager = player;
	}

}
