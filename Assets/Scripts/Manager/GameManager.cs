﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : SingletonMonoBehaviour<GameManager> {

	[HideInInspector] public bool IsStagePlaying = false;
	[HideInInspector] public bool IsSceneChangeing = false;
	public readonly string StageScelectSceneName = "StageSelect";

	[SerializeField] Image m_FadeImage;
	AudioSource m_AudioSource;
	bool IsFadeOut = false;
	bool m_IsPause = false;
	bool m_CanBGMPlay = true;
	int m_ResentPlayStageIndex = 0;
	float m_BGMVolume = 0.2f;
	float m_SEVolume = 0.9f;

	public override void Awake (){
		base.Awake ();
		SceneManager.sceneLoaded += OnSceneLoaded;
		m_AudioSource = GetComponent<AudioSource> ();
	}

	public bool IsPause{
		set{
			m_IsPause = value;
			Time.timeScale = m_IsPause ? 0f : 1f;
		}
		get{
			return m_IsPause;
		}
	}

	public int ResentPlayStageIndex{
		set{
			m_ResentPlayStageIndex = value;
		}
		get{
			return m_ResentPlayStageIndex;
		}
	}

	public float BGMVolume{
		set{ 
			m_BGMVolume = Mathf.Clamp01 (value);
		}
		get{ return m_BGMVolume; }
	}

	public float SEVolume{
		set{ m_SEVolume = Mathf.Clamp01 (value);}
		get{ return m_SEVolume;}
	}

	void Update(){
		m_AudioSource.volume = BGMVolume;
		if (m_CanBGMPlay && !m_AudioSource.isPlaying) {
			m_AudioSource.Play ();
		}else if(!m_CanBGMPlay){
			m_AudioSource.Stop ();
		}
	}

	void OnSceneLoaded(Scene scene,LoadSceneMode mode){
		if (scene.name == "Title" || scene.name == "StageSelect") {
			if (!m_CanBGMPlay) {
				m_CanBGMPlay = true;
			}
		} else {
			m_CanBGMPlay = false;
		}
	}

	public void SceneChange(string sceneName, float time, Color color){
		if(!IsSceneChangeing){
			StartCoroutine (SceneChangeCoroutine (sceneName,time,color));
		}
	}
		

	IEnumerator SceneChangeCoroutine(string sceneName,float time,Color color){
		IsSceneChangeing = true;
		m_FadeImage.gameObject.SetActive (true);
		color.a = 0;
		m_FadeImage.color = color;
		float t = 0;
		float deltaAlpha = 1f / (time / 2);
		Color c = m_FadeImage.color;
		while(t <= time / 2f){
			c.a += deltaAlpha * Time.deltaTime;
			m_FadeImage.color= c;
			t += Time.deltaTime;
			yield return null;
		}

		t = 0;
		c = m_FadeImage.color;
		SceneManager.LoadScene (sceneName);
		yield return null;

		while(t <= time / 2f){
			c.a -= deltaAlpha * Time.deltaTime;
			m_FadeImage.color = c;
			t += Time.deltaTime;
			yield return null;
		}
		m_FadeImage.gameObject.SetActive (false);
		IsSceneChangeing = false;
	}

	public void FadeOut(float time,float stopTime,Color color){
		if (!IsFadeOut) {
			StartCoroutine (FadeOutCoroutine (time,stopTime,color));
		}
	}

	IEnumerator FadeOutCoroutine(float time,float stopTime,Color color){
		IsFadeOut = true;
		m_FadeImage.gameObject.SetActive (true);
		color.a = 0;
		m_FadeImage.color = color;
		float t = 0;
		float deltaAlpha = 1f / (time / 2);
		Color c = m_FadeImage.color;
		while(t <= time / 2f){
			c.a += deltaAlpha * Time.deltaTime;;
			m_FadeImage.color= c;
			t += Time.deltaTime;
			yield return null;
		}
		t = 0;
		c = m_FadeImage.color;

		if (stopTime <= 0)  {
			yield return null;
		} else {
			yield return new WaitForSeconds(stopTime);
		}
		while(t <= time / 2f){
			c.a -= deltaAlpha * Time.deltaTime;
			m_FadeImage.color = c;
			t += Time.deltaTime;
			yield return null;
		}
		m_FadeImage.gameObject.SetActive (false);
		IsFadeOut = false;
	}

}
