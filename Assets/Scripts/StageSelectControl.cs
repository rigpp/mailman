﻿using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections;

public class StageSelectControl : MonoBehaviour {

	[SerializeField] ScrollRect m_ScrollRect;
	[SerializeField] RectTransform m_Contents;
	[SerializeField] float m_IconMoveSpeed = 10f;
	[SerializeField] AudioClip m_StartSE;
	[SerializeField] AudioClip m_SelectSE;
	[SerializeField] AudioClip m_BackSE;
	[SerializeField] AudioSource m_SEAudio;
	//[SerializeField] AudioSource m_BGMAudio;

	int m_StageIndex = 0;
	int m_MaxStageCount;

	public int StageIndex{
		set{
			if (value >= 0 && value < m_Contents.childCount) {
				m_StageIndex = value;
				m_ScrollRect.horizontalNormalizedPosition = (float)m_StageIndex / m_MaxStageCount;
			}
		}
		get{
			return m_StageIndex;
		}
	}

	void Awake(){
		m_MaxStageCount = m_Contents.childCount - 1;
	}

	void Start(){
		StageIndex = GameManager.Instance.ResentPlayStageIndex;
	}

	void Update(){
		m_ScrollRect.horizontalNormalizedPosition = Mathf.Lerp (m_ScrollRect.horizontalNormalizedPosition,(float)m_StageIndex / m_MaxStageCount,m_IconMoveSpeed * Time.deltaTime);
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			ClickRight ();
		}
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			ClickLeft ();
		}

		m_SEAudio.volume = GameManager.Instance.SEVolume;
		//m_BGMAudio.volume = GameManager.Instance.BGMVolume;

		if (CrossPlatformInputManager.GetButtonDown ("Jump")) {
			switch(StageIndex){
			case 0:
				SceneChange ("Stage1");
				break;
			case 1:
				SceneChange ("Stage2");
				break;
			case 2:
				SceneChange ("Stage3");
				break;
			}
		}
	}

	public void ClickRight(){
		if (m_StageIndex < m_MaxStageCount) {
			m_StageIndex++;
			m_SEAudio.PlayOneShot(m_SelectSE);
		}
	}

	public void ClickLeft(){
		if (m_StageIndex > 0) {
			m_StageIndex--;
			m_SEAudio.PlayOneShot (m_SelectSE);
		}
	}

	public void SceneChange(string sceneName){
		if (sceneName == "Title") {
			m_SEAudio.PlayOneShot (m_BackSE);
		} else {
			m_SEAudio.PlayOneShot (m_StartSE);
		}
		GameManager.Instance.SceneChange (sceneName, 2f, Color.black);
	}
}
